FROM node:slim
WORKDIR /app
COPY package.json .
COPY tsconfig.json .
COPY index.ts .
RUN npm install \
&& npm run build
EXPOSE 3000
CMD ["npm", "start"]